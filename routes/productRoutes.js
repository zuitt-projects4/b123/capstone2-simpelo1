//imports
const express = require("express")
const router = express.Router()
const auth = require('../auth')
const productController = require('../controllers/productControllers')

const {verify, verifyAdmin} = auth
const {
		getAllActiveProducts, getSingleProducts, getProductsByName, getProductsByPrice,
		addProduct,
		updateProduct, archiveProduct, activateProduct
	  } = productController

//GET
router.get('/activeProducts', getAllActiveProducts)//get active products from db
router.get('/singleProducts/:id', getSingleProducts)//get active products from db
router.get('/productName/:name', getProductsByName)//get active products from db
router.get('/productPrice/:price', getProductsByPrice)//get active products from db


//POST
router.post('/addProduct', verify, verifyAdmin, addProduct)//add products to db

//PUT
router.put('/:id', verify, verifyAdmin, updateProduct)//update products 
router.put('/archive/:id', verify, verifyAdmin, archiveProduct)//archive products 
router.put('/activate/:id', verify, verifyAdmin, activateProduct)//activate products 


//exports
module.exports = router