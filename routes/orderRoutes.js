//imports
const express = require("express")
const router = express.Router()
const auth = require('../auth')
const orderController = require('../controllers/orderControllers')

const {verify, verifyAdmin} = auth
const {
		getUserOrders, getallOrders, getOrders, getAllOrderByUser,
		createOrder
	  } = orderController

//GET
router.get('/getOrders', verify, getUserOrders)//retrieve orders of logged users
router.get('/getOrder/:id', verify, getOrders)//retrieve orders of logged users
router.get('/getAllOrders', verify, verifyAdmin, getallOrders)// admin retrieve all orders
router.get('/getAllOrder/:id', verify, verifyAdmin, getAllOrderByUser)// admin retrieve all orders
//POST
router.post('/checkout', verify, createOrder)//add order of logged users
//PUT

//exports
module.exports = router