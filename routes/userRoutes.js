//imports
const express = require("express")
const router = express.Router()
const auth = require('../auth')
const userController = require('../controllers/userControllers')


const {verify, verifyAdmin} = auth
const {getAllUsers, getSingleUsers,registerUser, loginUser, setAsAdmin} = userController

//GET
router.get('/getUsers', verify, verifyAdmin, getAllUsers)//Admin retrieve all users
router.get('/getUsersID/:id', verify, verifyAdmin, getSingleUsers)//Admin retrieve single users


//POST
router.post('/register', registerUser)//add user
router.post('/login', loginUser)//login user

// PUT
router.put('/setAsAdmin/:id',verify, verifyAdmin, setAsAdmin)//set user as Admin by admin

//exports
module.exports = router