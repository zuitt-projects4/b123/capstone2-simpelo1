//imports
const mongoose = require("mongoose")

//model
const orderSchema = new mongoose.Schema(
	{
		userId: String, 
		datePurchased: {
			type: Date,
			default: new Date()
		}, 
		products:[
			{
				productId: String,
				name: String, 
				quantity: Number,
				price: Number
			}
		],
		total: Number
	}
)

//exports
module.exports = mongoose.model("Order", orderSchema)