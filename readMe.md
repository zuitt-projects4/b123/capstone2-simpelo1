Project Name: Louise's E-Commerce API

Packages Required
	express
	mongoose
	cors
	bcrypt
	jsonwebtoken
	nodemon


Features:
	1. User Registration
	2. User Authentication 
	3. Set User as Admin (Admin ONLY)
	4. Retrieve all Active products
	5. Retrieve Single Products
	6. Create Products (Admin ONLY)
	7. Update Products Information (Admin ONLY)
	8. Archive Products (Admin ONLY)
	9. Create Order (Non-admin ONLY)
   10. Retrieve authenticated User's Order
   11. Retrieve all orders (Admin ONLY)
   12. Activate Products (Admin ONLY)
   13. Display Products per Order 
   14. Retrieve all orders per user (Admin ONLY)
   15. Retrieve Single Product name
   16. Retrieve Products by price
   17. Retrieve All User (Admin ONLY)
   18. Retrieve Single User (Admin ONLY)


Routes and Request Body:

User Registration:
	POST: http://localhost:4000/users/register

	BODY(JSON):
	{
		"firstName":"String",
		"lastName":"String",
		"email":"String",
		"password":"String",
		"mobileNo": "String"
	}

User Login 
	POST: http://localhost:4000/users/login

	BODY(JSON):
	{
		"email":"String",
		"password":"String"	
	}

Set as Admin
	POST: http://localhost:4000/users/setAsAdmin/id

	Admin's Token Required.
	BODY: No Request Body

	Admin Credentials: 
		{
			"email":"adminAPI@gmail.com",
			"password":"adminAPI123"	
		}

Retrieve all Active products:
	GET: http://localhost:4000/products/activeProducts
	
	BODY: No Request Body


Retrieve Single Product:
	GET: http://localhost:4000/products/singleProducts/id
	
	BODY: No Request Body


Create Product(Admin ONLY):
	POST: http://localhost:4000/products/addProduct

	Admin's Token Required
	BODY(JSON)
	{
	    "name": "String", 
	    "description": "String",
	    "price": Number
	}

Update Product(Admin ONLY):
	PUT: http://localhost:4000/products/id

	Admin's Token Required
	BODY(JSON)
	{
	    "name": "String", 
	    "description": "String",
	    "price": Number
	}


Archive Product(Admin ONLY):
	PUT: http://localhost:4000/products/archive/id

	Admin's Token Required
	BODY: No Request Body

Activate Product(Admin ONLY):
	PUT: http://localhost:4000/products/activate/id

	Admin's Token Required
	BODY: No Request Body

Create Order/Check-out
	POST: http://localhost:4000/orders/checkout

	User's Token Required
	BODY(JSON):
		{
		   "products":[
		       {
		           "productId": "string",
		           "name":"string",
		           "quantity": number,
		           "price": number
		       },
		       {
		           "productId": "string",
		           "name":"string",
		           "quantity": number,
		           "price": number
		       }
		   ],
		   "total": number 
		}

Retrieve authenticated User's Order
	GET: http://localhost:4000/orders/getOrders

	User's Token Required
	BODY: No Request Body


Retrieve all orders (Admin ONLY)
	GET: http://localhost:4000/orders/getAllOrders

	Admin's Token Required.
	BODY: No Request Body

 Display Products per Order (User ONLY)
 	GET: http://localhost:4000/orders/getOrder/id

	User's Token Required
	BODY: No Request Body

Retrieve all orders per user (Admin ONLY)
	GET: http://localhost:4000/orders/getAllOrder/userId

	Admin's Token Required
	BODY: No Request Body

Retrieve Single Product name
	GET: http://localhost:4000/products/productName/name
	
	BODY: No Request Body

Retrieve Products by price
	GET: http://localhost:4000/products/productPrice/500
	
	BODY: No Request Body

Retrieve All User (Admin ONLY)
	GET: http://localhost:4000/users/getUsers

	Admin's Token Required
	BODY: No Request Body


Retrieve Single User (Admin ONLY)
	GET: http://localhost:4000/users/getUsersID/id

	Admin's Token Required
	BODY: No Request Body