//imports
const express = require("express")
const mongoose = require("mongoose")
/*const cors = require("cors")*/

const app = express()
const port = process.env.PORT || 4000

//mongoose connection to MongoDB
mongoose.connect("mongodb+srv://louiseb123:gKg58tTCuFuIXHR@cluster0.o89xz.mongodb.net/Louise'sE-commerceAPI?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true, 
		useUnifiedTopology: true
	}
)

let db = mongoose.connection
db.on("error", console.error.bind(console, "Connection Error"))
db.once("open", ()=>console.log("Connected to MongoDB"))
app.use(express.json())

//user Routes
const userRoutes = require('./routes/userRoutes')
app.use('/users', userRoutes)

//Product Routers
const productRoutes = require('./routes/productRoutes')
app.use('/products', productRoutes)

//Order Routers
const orderRoutes = require('./routes/orderRoutes')
app.use('/orders', orderRoutes)

app.listen(port, ()=>console.log(`Server running at port ${port}`))