//imports
const Order = require('../models/Order')

//Methods
//GET
//User retrieve all orders
module.exports.getUserOrders = (req , res) => {

	Order.find({userId:req.user.id})
	.then(order => res.send(order))
	.catch(err => res.send(err))

}

// Admin retrieve all orders
module.exports.getallOrders = (req , res) => {

	Order.find()
	.then(order => res.send(order))
	.catch(err => res.send(err))

}

//User retrieve products by Order id
module.exports.getOrders = (req , res) => {

	Order.findById(req.params.id)
	.then(order => res.send(order))
	.catch(err => res.send(err))

}

//Admin retrieve all orders by user Id
module.exports.getAllOrderByUser = (req, res) =>{

	Order.find({userId:req.params.id})
	.then(order => res.send(order))
	.catch(err => res.send(err))

}

//POST
//create user's order
module.exports.createOrder = (req, res) => {

	
	if(req.user.isAdmin)return res.send({auth: "Failed.", message: "Action Forbidden"})




	let newOrder = new Order(
		{
			userId:req.user.id,
			total: req.body.total
		}
	)
	
	req.body.products.forEach(products => {newOrder.products.push(products)})
	console.log(newOrder)
	

	newOrder.save()
	.then(newProd => res.send(`Check-out Order.`))
	.catch(err => res.send(err))

}

