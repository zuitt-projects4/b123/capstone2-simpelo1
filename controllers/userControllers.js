//imports
const User = require('../models/Users')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const {createAccessToken} = auth

//GET
//admin retrieve list of users
module.exports.getAllUsers =(req, res) => {

	User.find()
	.then(result => res.send(result))
	.catch(err => res.send(err))


}

// admin retrieve single users by User Id 
module.exports.getSingleUsers =(req, res) => {

	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))

}



//POST
//registration for new user
module.exports.registerUser = (req, res) =>{

	if(req.body.password < 8 ) return res.send({message: "Password is too short."})
		
	const hashedPW = bcrypt.hashSync(req.body.password, 10)
	let newUser = new User(
		{
			firstName: req.body.firstName,
			lastName: req.body.lastName,
			mobileNo: req.body.mobileNo, 
			email: req.body.email,
			password: hashedPW
		}
	)

	newUser.save()
	.then(result => res.send({message:"Registration Successful."}))
	.catch(err => res.send(err))

}

//login in for users
module.exports.loginUser = (req, res) =>{

	
	User.findOne({email:req.body.email})
	.then(foundRes => {

		if(foundRes === null){
			return res.send({message: "No User Found"})
		}else{
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundRes.password)
			

			if(isPasswordCorrect){
				/*console.log("We will generate a key")*/
				return res.send({accessToken: createAccessToken(foundRes)})
			}else{
				return res.send({message: "Password is incorrect"})
			}
		}

	})
	.catch(err => res.send(err))
}

//PUT
//setting a user as admin
module.exports.setAsAdmin = (req, res) => {

	let updates ={isAdmin: true}

	User.findByIdAndUpdate(req.params.id, updates)
	.then(updatedUser => res.send({message: `Added a New Admin`}))
	.catch(err => res.send(err))

}