//imports
const Product = require('../models/Products')

//GET
// retrieve all active products
module.exports.getAllActiveProducts = (req, res) => {

	Product.find({isActive: true})
	.then(foundProduct => res.send(foundProduct))
	.catch(err => res.send(err))

}

//retrieve single products
module.exports.getSingleProducts = (req, res) => {

	Product.findById(req.params.id)
	.then(foundProduct => res.send(foundProduct))
	.catch(err => res.send(err))

}

//retrieve single products by name
module.exports.getProductsByName = (req, res) => {


	Product.find({name: {$regex:req.params.name, $options: '$i'}})
	.then(foundProduct => res.send(foundProduct))
	.catch(err => res.send(err))

}

//retrieve products by price
module.exports.getProductsByPrice = (req, res) => {

	let p = parseInt(req.params.price)

	Product.find({price: p})
	.then(foundProduct => res.send(foundProduct))
	.catch(err => res.send(err))

}



//POST
//admin user create products 
module.exports.addProduct = (req, res) => {

	Product.findOne({name: req.body.name})
	.then( foundProduct => {

		if(foundProduct !== null && foundProduct.name === req.body.name){
			res.send("Duplicate Products")
		}else{

			let newProduct = new Product(

					{
						name: req.body.name, 
						description: req.body.description,
						price : req.body.price
					}

				)
			newProduct.save()
			.then(newProd => res.send(`New Product Added`))
			.catch(err => res.send(err))

		}

	})
	.catch(err => res.send(err))
}

//PUT
//admin user update product info
module.exports.updateProduct = (req, res) => {

	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	}

	Product.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then( updatedProduct => res.send("Product Updated"))
	.catch(err => res.send(err))

}

//admin user archive products 
module.exports.archiveProduct = (req, res) => {

	let updates = {isActive: false}

	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(" Product Archived."))
	.catch(err => res.send(err))

}

//admin user activate products
module.exports.activateProduct = (req, res) => {

	let updates = {isActive: true}

	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(" Product Activated."))
	.catch(err => res.send(err))


}